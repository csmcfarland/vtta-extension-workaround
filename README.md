# Monster Parse Failures? Strahd/Argynvostholt importing problems?
I am not the developer of VTTA, but am trying to help people until they
do provide a formal fix for these issues.

## F.A.Q
1. **When will a formal fix be available?**
  
  I don't know and I don't know if the developer plans to come back and fix
  this module or release a new one or what. From what I know reading Patreon
  posts, they are focusing their time and effort into a new module structure
  and busy with real life.

2. **This didn't work for me! What should I do?**
  - First, try to follow the instructions again and make sure you
  didn't miss any steps.
  - Second, see if you can use [MrPrimate's importer](https://foundryvtt.com/packages/ddb-importer/).

# Summary of what this workaround does
You are going to make a copy of the VTTA Chrome Plugin, over-write some files,
and add your copied plugin folder back into Chrome.

## Assumptions
* You are using Chrome.
* You are familiar with copying and editing files on your computer.

# Instructions (Originally from Taodjin)
1. Open Chrome
1. Open 2 Tabs
1. First tab in Chrome:
   - Open: `chrome://version` in Chrome and note your Profile Path: information.
   (for Example: `C:\Users\yourusername\AppData\Local\Google\Chrome\User Data\Default`)
1. Open your Explorer open this Path
1. Open the folder: extensions
1. Second tab in Chrome:
   - Open `about:extensions` in that tap and turn on Developer Mode if it is not
   already active
   - Click on the details icon from VTTAssets: D&D Beyond & Foundry VTT
   - Look for ID (something like: mashfighiugseiafg)
1. Go back to your explorer and copy the Folder with the same arrangement of
letters
1. Paste it to your Desktop for example
1. Open it
1. Open folder 3.1.10_0
1. Open folder dndbeyond
1. Open monsters.js (you will need to use a text editor like Notepad)
1. Clear the document (it should be blank now)
1. Open the [monsters.js](https://gitlab.com/csmcfarland/vtta-extension-workaround/-/raw/master/monsters.js) link
1. Copy the text of the monsters.js file and paste it in the blank one from step 12
1. Open `about:extension` in Chrome and make sure Developer Mode is enabled.
1. Select Load unpacked and browse to the 3.1.10_0 folder in your copied
extension (for example on your desktop) ( you should see the folders dndbeyond,
icons, img, libs, static). Load this directory.

If you are having trouble importing scenes, you can try the same steps above, but also
update the copied extension with the [sources.js](https://gitlab.com/csmcfarland/vtta-extension-workaround/-/raw/master/sources.js) file.

Helpful Links:
- https://gist.github.com/paulirish/78d6c1406c901be02c2d

## Thanks
- Taodjin#2864 - For writing out alternative steps.
- Wurtz#2554 - For providing an updated `sources.js` that fixes the Curse of
  Strahd/Argynvostholt importing issue.
